<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
       
        $path = storage_path('people.json');
        $data = [];

        if(file_exists($path)){
            $inp = file_get_contents($path);
            $data = json_decode($inp, true);
        }
        return view('people', compact('data'));  
    }

    /**
     * Submiting a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){

        $validatedData = $request->validate([
        'first_name' => 'required',
        'surname' => 'required',
        ]);

        $path = storage_path('people.json');
        $first_name = $request->first_name;
        $surname = $request->surname;
        $inp = file_get_contents($path);
        $temArr = json_decode($inp, true);
        

        if(empty($temArr)){
            $nr = 1;
        }else{
            $last = end($temArr);
            $nr = $last['id']+1;
        }

        $array = [
            'id' => $nr,
            'first_name' => $first_name,
            'surname' => $surname
            ];


        if(!file_exists($path)){
            $secArray = [
            'id' => 1,
            'first_name' => $first_name,
            'surname' => $surname
            ];

            $userData[] = $secArray;
            $output = json_encode($userData);
            file_put_contents($path, $output);
            return response()->json($output, 200);
        }
        
        $temArr[] = $array;
        $userData = json_encode($temArr);
        file_put_contents($path, $userData);
        return response()->json($userData, 200);
    }
    /**
     * Updating an existing resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
         $validatedData = $request->validate([
        'first_name' => 'required',
        'surname' => 'required',
        ]);
        foreach ( $request->first_name as $key => $inp) {
            $array = [
            'id' => $request->id[$key],
            'first_name' => $inp,
            'surname' => $request->surname[$key]
             ];
            $item[] = $array;
        }
        $path = storage_path('people.json');
        unlink($path);
        $output = json_encode($item);
        file_put_contents($path, $output);
        return redirect()->route('home');
    }


    public function delete(Request $request, $id){
        $path = storage_path('people.json');
        $cont = file_get_contents($path);
        $inp = json_decode($cont, true);

        foreach ($inp as $key => $val) {
            if (in_array($id, $val)) {
                unset($inp[$key]);
            }
        }

        $test = json_encode($inp);

        file_put_contents($path, $test);
        return redirect()->route('home');
    }
}
