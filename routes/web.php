<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PeopleController@index')->name('home');
Route::post('/submit', 'PeopleController@create')->name('submit');
Route::post('/update', 'PeopleController@update')->name('update');
Route::post('/delete/{id}', 'PeopleController@delete')->name('delete');