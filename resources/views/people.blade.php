<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tech Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
    @if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form id="first_form" method="post"> <!-- first form used for creating new json data -->
      <div class="form-group">
        <label for="first_name">First Name:</label>
        <input type="text" class="form-control" name="first_name" id="first_name">
      </div>
      <div class="form-group">
        <label for="surname">Surname:</label>
        <input type="text" class="form-control" name="surname" id="surname">
      </div>
      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
        <form action="{{route('update')}} " method="POST"> <!-- second form used for update -->
        <table class="table">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Surname</th>
            </tr>
          </thead>
          <tbody>
          @if(!empty($data))
              @foreach($data as $row)
                  <tr>
                    <input type="hidden" name="id[]" value="{{$row['id']}}" />
                    <td><input type="text" name="first_name[]" value="{{$row['first_name']}}" /></td>
                    <td><input type="text" name="surname[]" value="{{$row['surname']}}" /></td>
                    <td>
                      <form action="{{route('delete', $row['id'])}}" method="POST">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <button type="submit"  onclick=" return confirm('Are you sure?');">
                            Delete
                      </form>
                    </td>
                  </tr>
              @endforeach
          @endif
          </tbody>
        </table> 
          <input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> <!-- csrf used againt possible vulnerabilities -->
          <button type="submit">Update</button>
        </form>
    </body>
</html>
<script type="text/javascript">
$("#first_form").submit(function(e) {
    var url = "{{route('submit')}}"; // the script where you handle the form input.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#first_form").serialize(), // we need this to serialize form elements.
           success: function(data)
           {
            var result;
            $.each(JSON.parse(data), function(row, value) {
                result+="<tr>"
                result+="<td>" + value.first_name + "</td><td>" + value.surname + "</td>";
                result+="</tr>";
            });
            $('tbody').html(result);
            location.reload();
           }
         });
    e.preventDefault(); //preventing the submit button for submitting the form.
});
</script>